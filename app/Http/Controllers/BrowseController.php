<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class BrowseController extends Controller {

    public function index(Request $request){
        $compacts = [];
        $reqQuery = $request->all();
        if(!empty($reqQuery)){
            foreach ($reqQuery as $key => $value) {
                $compacts[$key] = $value;
            }
            /* var_dump($request->all());
            $jbv = $request->jbv;
            if($jbv){
                $compacts[] = "jbv";
            } */
        }

        var_dump($compacts);
        //return View::make("components.jbv")->with("jbv", $compacts['jbv'])->render();
        return view("welcome", $compacts);
    }

}
