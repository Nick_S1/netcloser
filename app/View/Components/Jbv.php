<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Jbv extends Component {
    
    public $jbv;

    public function __construct($jbv) {
        $this->jbv = $jbv;
    }

    public function render() {
        return view('components.jbv');
    }
}
