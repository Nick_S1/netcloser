<?php

use App\Http\Controllers\BrowseController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('browse')->group(function () {
    Route::get('/users', function () {
        return "Hola";
    });
});

/* Route::get('browse', function (Request $request) {
    echo $request;
    return $request;
}); */

Route::get('/browse', [BrowseController::class, "index"]); // ->where('jbv', '[0-9]+')->name('jbv')