        <!-- <link rel="stylesheet" href="css/app.css">

        <script src="{{ asset('js/app.js') }}"></script> -->


        <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- <link rel="stylesheet" href="css/app.css"> -->
    <title>Netcloser</title>

    <style>
        body{
            background-color: #141414;
        }

        body p{
            color: white;
        }

        .item img{
            width: 100%
        }

        .list{
            padding: 0 4%;
        }

        .item{
            padding: 0 2px;
        }

        .item img{
            border-radius: 5px;
        }

        .carousel.slide:hover .carousel-indicators{
            display: flex !important;
            color: red;

            animation-name: fadeIn;
            animation-duration: .3s
        }

        .carousel-indicators button{
            background-color: red;
        }

        .carousel-control-next,
        .carousel-control-prev{
            width: auto;
            height: auto;
            background: rgba(20,20,20,.7);
        }

        body .carousel-control-prev-icon,
        body .carousel-control-next-icon{
            color: white;
        }

        body .no-padding{
            padding-left: 0;
            padding-right: 0;
        }

        @keyframes fadeIn {
            from {opacity: 0} 
            to {opacity: 1}
        }

        @keyframes fadeOut {
            from {opacity: 1} 
            to {opacity: 0}
        }

</style>
</head>
<body>
<!-- partial:index.partial.html -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <h1>Hello, world!</h1>
    <!-- Button trigger modal -->

    @isset($jbv)
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
            Launch demo modal
        </button>
        <x-jbv :jbv="$jbv"/>
    @endisset

    <div id="app">
        <app></app>
    </div>
<!-- 
    <div class="list">
        <h2 class="rowHeader">Tendencias ahora</h2>
        <div class="row">
            <div class="col-sm" style="padding: 0 2px;"><div class="item"><img src='https://static.wikia.nocookie.net/hazbinhotel/images/4/42/Helluva_Boss_Pilot.png'></div></div>
            <div class="col-sm" style="padding: 0 2px;"><div class="item"><img src='https://static.wikia.nocookie.net/hazbinhotel/images/4/42/Helluva_Boss_Pilot.png'></div></div>
            <div class="col-sm" style="padding: 0 2px;"><div class="item"><img src='https://static.wikia.nocookie.net/hazbinhotel/images/4/42/Helluva_Boss_Pilot.png'></div></div>
            <div class="col-sm" style="padding: 0 2px;"><div class="item"><img src='https://static.wikia.nocookie.net/hazbinhotel/images/4/42/Helluva_Boss_Pilot.png'></div></div>
            <div class="col-sm" style="padding: 0 2px;"><div class="item"><img src='https://static.wikia.nocookie.net/hazbinhotel/images/4/42/Helluva_Boss_Pilot.png'></div></div>
            <div class="col-sm" style="padding: 0 2px;"><div class="item"><img src='https://static.wikia.nocookie.net/hazbinhotel/images/4/42/Helluva_Boss_Pilot.png'></div></div>
        </div>
    </div>

    <div class="lista">
        <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel" data-bs-interval="false">
            <div class="carousel-indicators d-none">
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
            </div>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="row">
                        <div class="col-xs-2 col-sm-2 col-md-2 item">
                            <img src="./img/jbv/1/hero.jpg" class="d-block w-100" alt="...">
                            <div class="p-2">
                                <h5>First 1 slide label</h5>
                                <p>Some representative placeholder content for the first slide.</p>
                            </div>
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 item">
                            <img src="./img/jbv/1/hero.jpg" class="d-block w-100" alt="...">
                                <h5>First 2 slide label</h5>
                                <p>Some representative placeholder content for the first slide.</p>
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 item">
                            <img src="./img/jbv/1/hero.jpg" class="d-block w-100" alt="...">
                                <h5>First 3 slide label</h5>
                                <p>Some representative placeholder content for the first slide.</p>
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 item">
                            <img src="./img/jbv/1/hero.jpg" class="d-block w-100" alt="...">
                                <h5>First 4 slide label</h5>
                                <p>Some representative placeholder content for the first slide.</p>
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 item">
                            <img src="./img/jbv/1/hero.jpg" class="d-block w-100" alt="...">
                                <h5>First 4 slide label</h5>
                                <p>Some representative placeholder content for the first slide.</p>
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 item">
                            <img src="./img/jbv/1/hero.jpg" class="d-block w-100" alt="...">
                                <h5>First 4 slide label</h5>
                                <p>Some representative placeholder content for the first slide.</p>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="row">
                        <div class="col-xs-2 col-sm-2 col-md-2 item">
                            <img src="./img/jbv/1/hero.jpg" class="d-block w-100" alt="...">
                            <div class="p-2">
                                <h5>First 1 slide label</h5>
                                <p>Some representative placeholder content for the first slide.</p>
                            </div>
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 item">
                            <img src="./img/jbv/1/hero.jpg" class="d-block w-100" alt="...">
                                <h5>First 2 slide label</h5>
                                <p>Some representative placeholder content for the first slide.</p>
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 item">
                            <img src="./img/jbv/1/hero.jpg" class="d-block w-100" alt="...">
                                <h5>First 3 slide label</h5>
                                <p>Some representative placeholder content for the first slide.</p>
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 item">
                            <img src="./img/jbv/1/hero.jpg" class="d-block w-100" alt="...">
                                <h5>First 4 slide label</h5>
                                <p>Some representative placeholder content for the first slide.</p>
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 item">
                            <img src="./img/jbv/1/hero.jpg" class="d-block w-100" alt="...">
                                <h5>First 4 slide label</h5>
                                <p>Some representative placeholder content for the first slide.</p>
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 item">
                            <img src="./img/jbv/1/hero.jpg" class="d-block w-100" alt="...">
                                <h5>First 4 slide label</h5>
                                <p>Some representative placeholder content for the first slide.</p>
                        </div>
                    </div>
                </div><div class="carousel-item">
                    <div class="row">
                        <div class="col-xs-2 col-sm-2 col-md-2 item">
                            <img src="./img/jbv/1/hero.jpg" class="d-block w-100" alt="...">
                            <div class="p-2">
                                <h5>First 1 slide label</h5>
                                <p>Some representative placeholder content for the first slide.</p>
                            </div>
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 item">
                            <img src="./img/jbv/1/hero.jpg" class="d-block w-100" alt="...">
                                <h5>First 2 slide label</h5>
                                <p>Some representative placeholder content for the first slide.</p>
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 item">
                            <img src="./img/jbv/1/hero.jpg" class="d-block w-100" alt="...">
                                <h5>First 3 slide label</h5>
                                <p>Some representative placeholder content for the first slide.</p>
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 item">
                            <img src="./img/jbv/1/hero.jpg" class="d-block w-100" alt="...">
                                <h5>First 4 slide label</h5>
                                <p>Some representative placeholder content for the first slide.</p>
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 item">
                            <img src="./img/jbv/1/hero.jpg" class="d-block w-100" alt="...">
                                <h5>First 4 slide label</h5>
                                <p>Some representative placeholder content for the first slide.</p>
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 item">
                            <img src="./img/jbv/1/hero.jpg" class="d-block w-100" alt="...">
                                <h5>First 4 slide label</h5>
                                <p>Some representative placeholder content for the first slide.</p>
                        </div>
                    </div>
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions"  data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions"  data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </div> -->
<!-- partial -->

        <div class="container pt-4">
            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sapiente, nemo sint autem minus perferendis fuga veniam ut et laborum error sunt eligendi placeat nihil corrupti harum beatae aliquid eius fugit?</p>
            <p>Recusandae nesciunt ipsum eius omnis provident alias iure temporibus aspernatur harum exercitationem vitae velit nulla impedit qui aperiam, autem voluptates dolore unde laboriosam doloremque soluta similique praesentium ab. Voluptatibus, placeat.</p>
            <p>Nihil incidunt esse ut veritatis voluptatem quam et, labore explicabo autem, quod recusandae ex ipsa quas eius, unde iure atque ducimus nisi sequi placeat illum dolores quasi! Architecto, voluptatum eaque.</p>
            <p>Mollitia veritatis aliquid natus quisquam nihil nemo molestias nam qui temporibus provident dolorem ea numquam, autem voluptatibus illo quia? Aut veniam architecto aliquid pariatur totam mollitia laboriosam. Eaque, blanditiis? Dolorem!</p>
            <p>Magnam mollitia ab a distinctio beatae? Enim temporibus a sequi nobis iure eveniet laudantium possimus deleniti culpa ipsa, quam modi soluta tempora aliquid. Reprehenderit similique, dignissimos reiciendis aperiam vero incidunt.</p>
            <p>Quod minima ab et officiis possimus culpa corporis eaque modi optio impedit voluptatum magni tempore dignissimos aperiam, repellat temporibus eos vel praesentium veritatis numquam reprehenderit placeat provident ducimus. Molestias, amet?</p>
            <p>Veritatis, mollitia labore obcaecati quidem sunt placeat atque ad iure hic optio minima dolorem eius alias. Quisquam molestias deserunt maxime aliquam, corporis vero fugit nulla possimus suscipit deleniti, quos culpa!</p>
            <p>Nobis quam sapiente aliquam inventore quod sit? Expedita fuga, magni doloribus ab minus a in tempore, unde assumenda possimus illum natus molestias ex laudantium. Blanditiis inventore labore adipisci praesentium sunt.</p>
            <p>Quam consequuntur, in modi maiores quidem libero. Veniam laborum error architecto voluptas? Dolores, esse nihil provident a earum corrupti aliquid autem cumque doloribus quisquam ratione suscipit eos alias amet aut.</p>
            <p>Dolorum ipsum perspiciatis voluptas id, et ipsam provident rem facere architecto repellat veritatis eveniet voluptates ipsa quidem dignissimos eligendi nesciunt esse impedit eos inventore, libero ratione! Deleniti esse odit amet!</p>
            <p>Nesciunt mollitia sunt sequi earum perspiciatis fuga, ut, incidunt suscipit veritatis, distinctio tempora. Consequatur quos alias incidunt. Quaerat error doloremque maiores repudiandae aut explicabo. Distinctio quae sed eos earum nobis.</p>
            <p>Facere reiciendis saepe voluptate esse, earum impedit explicabo, modi eveniet minima deserunt provident, pariatur quia eaque nisi? Delectus libero perferendis mollitia itaque rem, unde magni dolores labore maiores consectetur voluptate?</p>
            <p>Libero sint at excepturi quod autem velit expedita omnis voluptatibus voluptate cum dolorem corrupti asperiores necessitatibus officia vel, fugit aperiam eius eum harum ex aut. Impedit sunt ipsam nostrum tempore!</p>
            <p>Dolor aliquid eius, sed soluta fuga dolore perspiciatis odit quaerat iusto impedit excepturi distinctio modi voluptas quam quas voluptatibus. Voluptate eveniet id accusantium non. Esse harum ipsam nostrum sint cupiditate.</p>
            <p>Ea nostrum veniam, reiciendis possimus deserunt neque corporis qui nobis vitae impedit eum fugit soluta omnis accusantium odit laborum dolorum. In laborum quaerat deleniti vero iusto. Ut ratione illum consequatur!</p>
            <p>Dolores placeat labore vel illum, magnam eum iste quaerat eveniet tempore corporis, dolor repellendus. Totam laborum saepe rerum fugiat. Quos quidem facere illum fugit distinctio a, fuga eaque sint omnis.</p>
            <p>Ratione officiis, temporibus rem sint optio impedit distinctio aspernatur. Officiis, praesentium atque. Totam obcaecati libero eum itaque magnam, animi est, odit esse, omnis aut iusto sunt suscipit nostrum tempora ratione!</p>
            <p>Eum facere voluptate est, aliquam sit rerum quibusdam nesciunt enim quae veniam soluta blanditiis possimus? Deserunt, quia non omnis, quos facilis eius alias eligendi quo, maxime dignissimos magnam maiores quis.</p>
            <p>Necessitatibus officiis pariatur maxime sit at quidem veritatis ad nam perspiciatis dolorem libero exercitationem sint quaerat voluptatum soluta, quis laudantium, numquam, et ipsam voluptate assumenda corporis iure? Incidunt, accusamus enim.</p>
            <p>Sunt error neque rerum porro ratione est, distinctio beatae quod officia deserunt excepturi asperiores vitae necessitatibus saepe quo, tempora cum quisquam quibusdam dolore? Vero, necessitatibus numquam corrupti voluptatum quos totam?</p>
            <p>Magnam nemo earum debitis nesciunt explicabo! Voluptatem officia commodi accusamus, iste quisquam libero esse atque et saepe, assumenda necessitatibus tempora repellat, delectus soluta. Voluptatem cumque atque quae maxime sed optio.</p>
            <p>Ducimus incidunt architecto nam aliquam magnam eius? Ducimus recusandae laudantium nobis reiciendis harum repellat reprehenderit accusantium quam corrupti tenetur, eum exercitationem, dignissimos vitae explicabo error, vero porro. Itaque, maiores magni?</p>
            <p>Nulla ipsa nihil rem iure. Ipsam delectus fugiat dolores dolor hic, tempore vel illo at enim quisquam minima consequuntur atque! Eligendi iste numquam facere atque dolorem soluta, pariatur libero modi.</p>
            <p>Ab ad aliquid quaerat, fuga nam ipsa non numquam fugiat ullam sequi quod, dicta exercitationem, animi aut quis temporibus vitae veritatis commodi optio saepe nulla consequuntur earum! Nesciunt, eius magnam?</p>
            <p>Quam nihil delectus, velit molestias ab necessitatibus ullam saepe debitis, sapiente eligendi praesentium, voluptatibus quas perspiciatis aliquid quia maiores impedit culpa excepturi sunt ea ut quos iste. Eius, ut laudantium.</p>
            <p>Architecto, doloribus, quia praesentium eius quae quis dolores corporis tempora eos fugit cumque aliquam alias beatae voluptatum possimus hic ratione consequuntur incidunt eveniet! Distinctio aut, esse vitae magni facere autem!</p>
            <p>Fugiat sapiente, consequuntur accusamus neque totam repudiandae perspiciatis sit inventore modi, deserunt, voluptatibus nobis amet et? Quasi dolor dolorem omnis fugit ipsam numquam, distinctio totam vitae iure assumenda rerum accusamus!</p>
            <p>Nam incidunt minima necessitatibus blanditiis eaque. Ut velit exercitationem nam eaque. Sapiente quis magni facilis repudiandae, quaerat doloremque temporibus aliquam, iste maxime ad dolores laborum soluta, illum asperiores labore architecto.</p>
            <p>Maiores dolorem sunt dicta consectetur mollitia ullam voluptas. Quia, odio et? Voluptas maiores, modi incidunt nobis explicabo similique suscipit, fugit architecto sapiente neque, distinctio facere recusandae ea. Blanditiis, magni maiores.</p>
            <p>Molestias excepturi aliquid reprehenderit obcaecati similique. Aut perferendis pariatur natus facilis, incidunt, magnam obcaecati animi voluptas nisi molestiae tenetur minima enim deleniti amet. Quas voluptas debitis est! Eius, totam vel!</p>
        </div>
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>

    <script>

        function getParentNode(child, parentClass) {
            var node = child.parentNode;
            if(child.className == parentClass){
                return child;
            }
            while (node != null) {
                if (node.className == parentClass) {
                    return node;
                }
                node = node.parentNode;
            }
            return null;
        }

        function HorizontallyBound($parentDiv, $childDiv) {
            var parentRect = $parentDiv[0].getBoundingClientRect();
            var childRect = $childDiv[0].getBoundingClientRect();

            console.log(parentRect, childRect);

            return parentRect.right < (childRect.right + childRect.width) || parentRect.right <= childRect.left;
        }

        class MiniModal{

            div = null;

            constructor(item, id){
                this.item = item;
                this.id = id;
            }

            render(){                                    
                this.div = `<div id="${this.id}" class="minimodal" 
                                style="position:absolute; margin: 0 auto;left: ${this.item.position()['left']}px; top: ${this.item.position()['top']}px; 
                                width: ${this.item.width()}px; height: ${this.item.height()}px; background-color: #999; color: white; 
                                opacity:0">
                                    <div class="row">
                                        
                                    </div>Pepe ${this.id}</div>`;
                $("body").append(this.div);

                setTimeout(() => {
                    if((this.item.position()['left'] < this.item.width())){
                        $("#" + this.id).animate({ margin: -this.item.width()/4, "margin-left": 0, width: "+=100", height: "+=100", opacity: 1});
                    }else if(HorizontallyBound($(getParentNode(this.item[0], "list")), this.item)) {
                        $("#" + this.id).animate({ margin: -this.item.width()/4, "margin-left": -100, width: "+=100", height: "+=100", opacity: 1});
                    }else{
                        $("#" + this.id).animate({ margin: -this.item.width()/4, width: "+=100", height: "+=100", opacity: 1});
                    }
                }, 500);
                $(this.div).hide();
            }

            hide(){
                $('#'+this.id).remove();
            }
        }

        $( document ).ready(function() {

            /* $('.item')
            .on('mouseenter', function(){
                $(this).animate({ width: "+=250", height: "+=250" });
            })
            .on('mouseleave', function(){
                $(this).animate({ width: "-=250", height: "-=250" });
            }) */
            var minimodal = null;

            $('.item').hover(function(){
                if(minimodal == null){
                    minimodal = new MiniModal($(this), new Date().getMilliseconds());
                    minimodal.render();
                }
            }, function () {
                $('.minimodal').hover(function(){
                }, function () {
                    if(minimodal != null){
                        minimodal.hide();
                        minimodal = null;
                    }
                    console.log("exit minimodal");
                });
            }); 

            
        });
    </script>

</body>
</html>