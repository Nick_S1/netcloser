// Get the modal

function getParentNode(child, parentClass) {
    var node = child.parentNode;
    if(child.className == parentClass){
        return child;
    }
    while (node != null) {
        if (node.className == parentClass) {
            return node;
        }
        node = node.parentNode;
    }
    return null;
}

$( document ).ready(function() {
    $('.close').click(function(){
        let modal = getParentNode(this, "modal");
        if(modal != null)
            closeVideoModal(modal);
    })
});

function generatedModal(){
    $('body').toggleClass('modal-open');
}

function closeVideoModal(modal){
    if(modal != null){
        if (modal.style.display === "none") {
            modal.style.display = "block";
        } else {
            modal.classList.add("fadeOut");
            setTimeout(() => {
                modal.style.display = "none";
            }, 290);
        }
        setTimeout(() => {
            $('body').toggleClass('modal-open');
        }, 300);
        /*

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        } */
    }
}